const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ingredientSchema = new Schema({
  ingName: {type: String, required: true},
  amount: {type: Number, required: true},
  unit: {type: String, required: true},
});

const CocktailSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  image: String,
  recipe: {
    type: String,
    required: true,
  },
  isPublished: {
    type: Boolean,
    default: false,
  },
  ingredients : [
    ingredientSchema
  ],
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;