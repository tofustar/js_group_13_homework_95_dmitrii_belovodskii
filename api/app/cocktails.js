const express = require('express');
const fs = require('fs').promises;
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const Cocktail = require('../models/Cocktail');
const mongoose = require("mongoose");


const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try{
    const filter = {};

    if(req.query.user) {
      filter.user = req.query.user;
    }

    const cocktails = await Cocktail.find(filter);

    console.log(cocktails);
    return res.send(cocktails);
  } catch (e) {
    return next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {

    const cocktail = await Cocktail.findById(req.params.id);

    if(!cocktail) {
      return res.status(404).send({message: 'Not found'});
    }

    console.log(cocktail);

    return res.send(cocktail);
  } catch (e) {
    return next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    const cocktailData = {
      user: req.user,
      name: req.body.name,
      image: null,
      recipe: req.body.recipe,
      isPublished: false,
      ingredients: [{
        ingName: req.body.ingredients.ingName,
        amount: req.body.ingredients.amount,
        unit: req.body.ingredients.unit
      }],
    };

    console.log(cocktailData);

    if(req.file) {
      cocktailData.image = req.file.filename;
    }

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    return res.send({message: 'Created new cocktail', id: cocktail._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      if(req.file) {
        await fs.unlink(req.file.path);
      }

      return res.status(400).send(e);
    }

    next(e);
  }
});

module.exports = router;