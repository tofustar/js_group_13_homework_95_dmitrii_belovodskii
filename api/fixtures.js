const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const Cocktail = require("./models/Cocktail");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [John, Jain, Admin] = await User.create({
      email: 'john@test.com',
      password: '123',
      token: nanoid(),
      displayName: 'John',
      avatar: 'user-icon.png',
      role: 'user'
    },
    {
      email: 'jain@test.com',
      password: 'qwe',
      token: nanoid(),
      displayName: 'Jain',
      avatar: 'user-icon.png',
      role: 'user'
    },

    {
      email: 'admin@test.com',
      password: 'zxc',
      token: nanoid(),
      displayName: 'Admin',
      avatar: 'admin-icon.png',
      role: 'admin'
    })

  await Cocktail.create({
      user: Jain,
      name: 'Mojito',
      image: 'mojito.jpg',
      recipe: 'Place 3 lime wedges in a highball glass and muddle.\n' +
        'Take 10 mint leaves in one hand and slap them with the other hand.\n' +
        'Put a mint in a highball.\n' +
        'Fill a glass to the top with crushed ice.\n' +
        'Add sugar syrup 15 ml and white rum 50 ml.\n' +
        'Top up with club soda and gently stir with a cocktail spoon.\n' +
        'Add some crushed ice.\n' +
        'Garnish with mint sprig and lime wedge.',
      isPublished: true,
      ingredients: [
        {
          ingName: 'White rum',
          amount: 50,
          unit: 'ml',
        },
        {
          ingName: 'Sugar syrup',
          amount: 15,
          unit: 'gr',
        },
        {
          ingName: 'Soda',
          amount: 100,
          unit: 'ml',
        },
        {
          ingName: 'Lime',
          amount: 80,
          unit: 'gr',
        },
        {
          ingName: 'Mint',
          amount: 3,
          unit: 'pcs',
        },
        {
          ingName: 'Ice',
          amount: 200,
          unit: 'gr',
        }
      ]
    },
    {
      user: John,
      name: 'B-52',
      image: 'b-52.jpg',
      recipe: 'Pour 15 ml of coffee liqueur into the glass.\n' +
        'Using a cocktail spoon, layer on 15 ml of Irish cream and 15 ml of Triple sec liqueur.\n' +
        'Set it on fire, arm yourself with straws and treat!.',
      isPublished: true,
      ingredients: [
        {
          ingName: 'Coffee liqueur',
          amount: 15,
          unit: 'ml',
        },
        {
          ingName: 'irish cream',
          amount: 15,
          unit: 'ml',
        },
        {
          ingName: 'Triple sec liqueur',
          amount: 15,
          unit: 'ml',
        }
      ]
    },
    {
      user: Jain,
      name: 'Gin Tonic',
      image: 'gin-tonic.jpg',
      recipe: 'Fill your highball glass to the top with ice cubes.\n' +
        'Pour gin 50 ml.\n' +
        'Pour the tonic to the top and gently stir with a cocktail spoon.\n' +
        'Garnish with lime slice.',
      is_published: true,
      ingredients: [
        {
          ingName: 'Gin',
          amount: 50,
          unit: 'ml',
        },
        {
          ingName: 'Tonic',
          amount: 150,
          unit: 'ml',
        },
        {
          ingName: 'Lime',
          amount: 20,
          unit: 'gr',
        },
        {
          ingName: 'Ice cubes',
          amount: 180,
          unit: 'gr',
        }
      ]
    },
    {
      user: Jain,
      name: 'Blue Lagoon',
      image: 'blue-lagoon.jpg',
      recipe: 'Fill the hurricane with ice cubes.\n' +
        'Pour in 20 ml blue curacao liqueur and 50 ml vodka.\n' +
        'Pour Sprite to the top and gently stir with a cocktail spoon.\n' +
        'Garnish with a pineapple wedge.',
      isPublished: true,
      ingredients: [
        {
          ingName: 'Vodka',
          amount: 50,
          unit: 'ml',
        },
        {
          ingName: 'Blue curacao liqueur',
          amount: 15,
          unit: 'ml',
        },
        {
          ingName: 'Sprite',
          amount: 150,
          unit: 'ml',
        },
        {
          ingName: 'Pineapple',
          amount: 30,
          unit: 'gr',
        },
        {
          ingName: 'Ice cubes',
          amount: 200,
          unit: 'gr',
        }
      ]
    },
    {
      user: Admin,
      name: 'Manhattan',
      image: 'manhattan.jpg',
      recipe: 'Pour 25 ml of red vermouth and 50 ml of bourbon into a mixing glass.\n' +
        'Add angostura bitter 1 dash.\n' +
        'Fill a glass with ice cubes and stir with a cocktail spoon.\n' +
        'Pour through a strainer into a chilled cocktail glass.\n' +
        'Garnish with a cocktail cherry on a skewer.',
      isPublished: true,
      ingredients: [
        {
          ingName: 'Bourbon',
          amount: 50,
          unit: 'ml',
        },
        {
          ingName: 'Red vermouth',
          amount: 25,
          unit: 'ml',
        },
        {
          ingName: 'Cherry',
          amount: 5,
          unit: 'pcs',
        },
        {
          ingName: 'Ice cubes',
          amount: 80,
          unit: 'gr',
        }
      ]
    },
    )

  await mongoose.connection.close();
};

run().catch(e => console.error(e));