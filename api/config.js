const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db:'mongodb://localhost/cocktail-recipes',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '378294564148828',
    appSecret: '2a01270ac1d222d3a989c45fb7eaddd2'
  }
};