export class Cocktail {
  constructor(
    public _id: string,
    public name: string,
    public image: string,
    public recipe: string,
    public isPublished: boolean,
    public ingredients: [
      {
        ingName: string,
        amount: number,
        unit: string,
      }
    ]
  ) {}
}

export interface ApiCocktailData {
    _id: string,
    name: string,
    image: string,
    recipe: string,
    isPublished: boolean,
    ingredients: [
      {
        ingName: string,
        amount: number,
        unit: string,
      }
    ],
}

export interface CocktailData {
  [key: string]: any,
  name: string,
  image: File | null,
  recipe: string,
  ingredients: [
    {
      ingName: string,
      amount: number,
      unit: string,
    }
  ]
}