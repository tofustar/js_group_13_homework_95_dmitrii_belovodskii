export interface User {
  _id: string,
  email: string,
  token: string,
  displayName: string,
  avatar: string,
  role: string,
}

export interface RegisterUserData {
  email: string,
  password: string,
  displayName: string,
  avatar: string,
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface LoginUserData {
  email: string,
  password: string,
}

export interface fbLoginUserData {
  authToken: string,
  id: string,
  email: string,
  displayName: string,
  avatar: string,
}

export interface LoginError {
  error: string
}