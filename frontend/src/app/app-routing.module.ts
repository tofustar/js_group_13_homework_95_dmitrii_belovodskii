import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegisterComponent} from "./pages/register/register.component";
import {LoginComponent} from "./pages/login/login.component";
import {CocktailsComponent} from "./pages/cocktails/cocktails.component";
import {CocktailInfoComponent} from "./pages/cocktail-info/cocktail-info.component";
import {NewCocktailComponent} from "./pages/new-cocktail/new-cocktail.component";
import {RoleGuardService} from "./services/role-guard.service";
import {MyCocktailsComponent} from "./pages/my-cocktails/my-cocktails.component";

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {path: 'cocktails', component: CocktailsComponent},
  {path: 'my-cocktails/:id', component: MyCocktailsComponent},
  {
    path: 'cocktails/new',
    component: NewCocktailComponent,
    canActivate: [RoleGuardService],
    data: {roles: ['admin', 'user']}
  },
  {path: 'cocktails/:id', component: CocktailInfoComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
