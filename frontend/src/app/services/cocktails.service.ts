import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment as env} from "../../environments/environment";
import {ApiCocktailData, Cocktail, CocktailData} from "../models/cocktail.model";

@Injectable({
  providedIn: 'root'
})

export class CocktailsService{

  constructor(private http: HttpClient) {}

  getCocktails() {
    return this.http.get<ApiCocktailData[]>(env.apiUrl + '/cocktails')
  }

  getCocktailsByUser(userId: string) {
    return this.http.get<ApiCocktailData[]>(env.apiUrl + '/cocktails?user=' + userId);
  }

  getCocktailInfo(cocktailId: string) {
    return this.http.get<Cocktail>(env.apiUrl + '/cocktails/' + cocktailId);
  }

  createCocktail(cocktailData: CocktailData) {
    const formData = new FormData();

    Object.keys(cocktailData).forEach(key => {
      if (cocktailData[key] !== null) {
        formData.append(key, cocktailData[key]);
      }
    });

    return this.http.post(env.apiUrl + '/cocktails', formData);
  }
}