import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {LayoutComponent} from './ui/layout/layout.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatListModule} from "@angular/material/list";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatButtonModule} from "@angular/material/button";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {RegisterComponent} from './pages/register/register.component';
import {LoginComponent} from './pages/login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {CenteredCardComponent} from './ui/centered-card/centered-card.component';
import {MatCardModule} from "@angular/material/card";
import {FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule} from "angularx-social-login";
import {environment as env} from "../environments/environment";
import {AppStoreModule} from "./app-store.module";
import {CocktailsComponent} from './pages/cocktails/cocktails.component';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {ImagePipe} from "./pipes/image.pipe";
import {CocktailInfoComponent} from './pages/cocktail-info/cocktail-info.component';
import {NewCocktailComponent} from './pages/new-cocktail/new-cocktail.component';
import {FileInputComponent} from "./ui/file-input/file-input.component";
import {AuthInterceptor} from "./auth.interceptor";
import {HasRolesDirective} from "./directives/has-roles.directive";
import {MatSelectModule} from "@angular/material/select";
import { MyCocktailsComponent } from './pages/my-cocktails/my-cocktails.component';

const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      provider: new FacebookLoginProvider(env.fbClientId, {
        scope: 'email,public_profile',
      })
    }
  ]
};

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    FileInputComponent,
    HasRolesDirective,
    RegisterComponent,
    LoginComponent,
    CenteredCardComponent,
    CocktailsComponent,
    ImagePipe,
    CocktailInfoComponent,
    NewCocktailComponent,
    MyCocktailsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    AppStoreModule,
    SocialLoginModule,
    MatProgressSpinnerModule,
    MatSelectModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
