import {LoginError, RegisterError, User} from "../models/user.model";
import {Cocktail} from "../models/cocktail.model";

export type CocktailsState = {
  cocktails: Cocktail[],
  cocktailInfo: null | Cocktail,
  cocktailsByUserId: string,
  cocktailId: string,
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type AppState = {
  users: UsersState,
  cocktails: CocktailsState,
}