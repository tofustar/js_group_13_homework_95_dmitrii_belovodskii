import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {CocktailsService} from "../services/cocktails.service";
import {
  createCocktailFailure,
  createCocktailRequest, createCocktailSuccess,
  fetchCocktailInfoFailure,
  fetchCocktailInfoRequest, fetchCocktailInfoSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess, fetchUserCocktailsFailure, fetchUserCocktailsRequest, fetchUserCocktailsSuccess
} from "./cocktails.actions";
import {catchError, map, mergeMap, of, tap} from "rxjs";
import {Router} from "@angular/router";
import {HelpersService} from "../services/helpers.service";

@Injectable()
export class CocktailsEffects {
  constructor(
    private actions: Actions,
    private cocktailsService: CocktailsService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchCocktails = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailsRequest),
    mergeMap(() => this.cocktailsService.getCocktails().pipe(
      map(cocktails => fetchCocktailsSuccess({cocktails})),
      catchError(() => of(fetchCocktailsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchCocktailsByUserId = createEffect(() => this.actions.pipe(
    ofType(fetchUserCocktailsRequest),
    mergeMap(({idUser}) => this.cocktailsService.getCocktailsByUser(idUser).pipe(
      map(cocktails => fetchUserCocktailsSuccess({cocktails})),
      catchError(() => of(fetchUserCocktailsFailure({error: 'Something went wrong'})))
    ))
  ));

  fetchCocktailInfo = createEffect(() => this.actions.pipe(
    ofType(fetchCocktailInfoRequest),
    mergeMap(({cocktailId}) => this.cocktailsService.getCocktailInfo(cocktailId).pipe(
      map(cocktailInfo => fetchCocktailInfoSuccess({cocktailInfo})),
      catchError(() => of(fetchCocktailInfoFailure({error: 'Something went wrong'})))
    ))
  ));

  createCocktail = createEffect(() => this.actions.pipe(
    ofType(createCocktailRequest),
    mergeMap(({cocktailData}) => this.cocktailsService.createCocktail(cocktailData).pipe(
      map(() => createCocktailSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Your cocktail is being reviewed by a admin');
        void this.router.navigate(['/']);
      }),
      catchError(() => of(createCocktailFailure({error: 'Wrong data'})))
    ))
  ));
}