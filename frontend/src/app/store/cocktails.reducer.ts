import {CocktailsState} from "./types";
import {createReducer, on} from "@ngrx/store";
import {
  createCocktailFailure,
  createCocktailRequest,
  createCocktailSuccess,
  fetchCocktailInfoFailure,
  fetchCocktailInfoRequest,
  fetchCocktailInfoSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchUserCocktailsFailure,
  fetchUserCocktailsRequest,
  fetchUserCocktailsSuccess
} from "./cocktails.actions";

const initialState: CocktailsState = {
  cocktails: [],
  cocktailInfo: null,
  cocktailsByUserId: '',
  cocktailId: '',
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};

export const cocktailsReducer = createReducer(
  initialState,
  on(fetchCocktailsRequest, state => ({...state, fetchLoading: true})),
  on(fetchCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUserCocktailsRequest, (state, {idUser}) => ({...state, fetchLoading: true, cocktailsByUserId: idUser})),
  on(fetchUserCocktailsSuccess, (state, {cocktails}) => ({...state, fetchLoading: false, cocktails})),
  on(fetchUserCocktailsFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchCocktailInfoRequest, (state, {cocktailId}) => ({...state, fetchLoading: true, cocktailId})),
  on(fetchCocktailInfoSuccess, (state, {cocktailInfo}) => ({...state, fetchLoading: false, cocktailInfo})),
  on(fetchCocktailInfoFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createCocktailRequest, state => ({...state, createLoading: true})),
  on(createCocktailSuccess, state => ({...state, createLoading: false})),
  on(createCocktailFailure, (state, {error}) => ({...state, createLoading: false,
  createError: error,})),
)