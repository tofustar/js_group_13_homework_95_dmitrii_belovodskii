import {createAction, props} from "@ngrx/store";
import {Cocktail, CocktailData} from "../models/cocktail.model";

export const fetchCocktailsRequest = createAction('[Cocktails] Fetch Request');
export const fetchCocktailsSuccess = createAction('[Cocktails] Fetch Success', props<{cocktails: Cocktail[]}>());
export const fetchCocktailsFailure = createAction('[Cocktails] Fetch Failure', props<{error: string}>());

export const fetchUserCocktailsRequest = createAction('[Cocktails] Fetch User Request', props<{idUser: string}>());
export const fetchUserCocktailsSuccess = createAction('[Cocktails] Fetch User Success', props<{cocktails: Cocktail[]}>());
export const fetchUserCocktailsFailure = createAction('[Cocktails] Fetch User Failure', props<{error: string}>());

export const fetchCocktailInfoRequest = createAction('[Cocktails] Fetch Info Request', props<{cocktailId: string}>());
export const fetchCocktailInfoSuccess = createAction('[Cocktails] Fetch Info Success', props<{cocktailInfo: Cocktail}>());
export const fetchCocktailInfoFailure = createAction('[Cocktails] Fetch Info Request', props<{error: string}>());


export const createCocktailRequest = createAction('[Cocktails] Create Request', props<{cocktailData: CocktailData}>());
export const createCocktailSuccess = createAction('[Cocktails] Create Success');
export const createCocktailFailure = createAction('[Cocktails] Create Failure', props<{error: string}>());