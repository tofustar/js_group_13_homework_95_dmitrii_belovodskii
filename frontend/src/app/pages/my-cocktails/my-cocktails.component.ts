import { Component, OnInit } from '@angular/core';
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {Cocktail} from "../../models/cocktail.model";
import {fetchUserCocktailsRequest} from "../../store/cocktails.actions";

@Component({
  selector: 'app-my-cocktails',
  templateUrl: './my-cocktails.component.html',
  styleUrls: ['./my-cocktails.component.sass']
})
export class MyCocktailsComponent implements OnInit {
  idUser: Observable<string>;
  cocktails: Observable<Cocktail[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.idUser = store.select(state => state.cocktails.cocktailsByUserId);
    this.cocktails = store.select(state => state.cocktails.cocktails);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);
  }

  ngOnInit(): void {
    const idUser = this.route.snapshot.params['id'];
    this.store.dispatch(fetchUserCocktailsRequest({idUser}));
  }

}
