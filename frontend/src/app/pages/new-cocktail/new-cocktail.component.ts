import {Component, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {CocktailData} from "../../models/cocktail.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {createCocktailRequest} from "../../store/cocktails.actions";

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.sass']
})
export class NewCocktailComponent implements OnInit {
  cocktailForm!: FormGroup;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.cocktails.createLoading);
    this.error = store.select(state => state.cocktails.createError);
  }

  ngOnInit() {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      recipe: new FormControl('', Validators.required),
      ingredients: new FormArray([], Validators.required),
      image: new FormControl(''),
    });
  }

  onSubmit() {
    const cocktailData: CocktailData = this.cocktailForm.value;
    console.log(cocktailData);
    this.store.dispatch(createCocktailRequest({cocktailData}));
  }

  addIng() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingGroup = new FormGroup({
      ingName: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      unit: new FormControl('', Validators.required)
    })
    ingredients.push(ingGroup);
  }

  getIngControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  removeIng(i: number) {
    const ing = <FormArray>this.cocktailForm.get('ingredients');
    ing.removeAt(i);
  }
}
