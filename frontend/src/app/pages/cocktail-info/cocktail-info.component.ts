import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Cocktail} from "../../models/cocktail.model";
import {AppState} from "../../store/types";
import {Store} from "@ngrx/store";
import {ActivatedRoute} from "@angular/router";
import {fetchCocktailInfoRequest} from "../../store/cocktails.actions";

@Component({
  selector: 'app-cocktail-info',
  templateUrl: './cocktail-info.component.html',
  styleUrls: ['./cocktail-info.component.sass']
})
export class CocktailInfoComponent implements OnInit {
  cocktailId: Observable<string>
  cocktailInfo: Observable<null | Cocktail>
  loading: Observable<boolean>
  error: Observable<null | string>

  cocktail!: Cocktail | null;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.cocktailId = store.select(state => state.cocktails.cocktailId);
    this.cocktailInfo = store.select(state => state.cocktails.cocktailInfo);
    this.loading = store.select(state => state.cocktails.fetchLoading);
    this.error = store.select(state => state.cocktails.fetchError);

  }

  ngOnInit(): void {
    const cocktailId = this.route.snapshot.params['id'];
    this.store.dispatch(fetchCocktailInfoRequest({cocktailId}));

    this.cocktailInfo.subscribe(cocktail => {
      this.cocktail = cocktail;
      console.log(this.cocktail);
    })
  }

}
